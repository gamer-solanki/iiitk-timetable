package com.admin.login;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class LoginDAO { 
	static Connection currentCon = null; 
	static ResultSet rs = null; 
	public static LoginParameters login(LoginParameters bean) { 
		//preparing some objects for connection 
		Statement stmt = null; 
		String username = bean.getId(); 
		String pass = bean.getPassword();
		//String password=UtilsSecure.encrypt(pass);
		String password=pass;

//code added
		char[] newpass1 = new char[password.length()];
		for(int i=0;i<2*(password.length());i++){
			if(i>0&&i<password.length()-1)
			{newpass1[i]=password.charAt(i);
			newpass1[i+1]=password.charAt(2);
			}
		}
		String b = new String(newpass1);
		System.out.println(b);
		//password=b;
//code added
		String searchQuery =  "SELECT * FROM admin WHERE userid = '"+username+"' AND password = '"+password+"' "; 
		// "System.out.println" prints in the console; Normally used to trace the process 
		System.out.println("Your user name is " + username); 
		System.out.println("Your password is " + password); 
		System.out.println("Query: "+searchQuery); 
		
		try { 
			//connect to DB 
			currentCon = ConnectionManager.getConnection(); 
			stmt=currentCon.createStatement(); 
			rs = stmt.executeQuery(searchQuery); 
			boolean more = rs.next(); 
			// if user does not exist set the isValid variable to false 
			
			if (!more) { 
				System.out.println("Sorry, you are not a registered user! Please sign up first"); 
				bean.setValid(false); } //if user exists set the isValid variable to true 
			
			else if (more) { 
				String id = rs.getString("userid"); 
				String name = rs.getString("name"); 
				System.out.println("Welcome " + name+" "+ id ); 
				bean.setId(id); 
				bean.setName(name); 
				bean.setValid(true); 
				} 
			} 
		catch (Exception ex) { 
				System.out.println("Log In failed: An Exception has occurred! " + ex); 
				} 
		//some exception handling 
		finally {
			if (rs != null) { 
				try { 
					rs.close(); 
					} 
				catch (Exception e) {
					
				} 
				rs = null; 
				} 
			if (stmt != null) { 
				try { stmt.close(); 
				} 
				catch (Exception e) {
					
				} stmt = null; 
				} 
			if (currentCon != null) { 
				try { 
					currentCon.close(); 
					} 
				catch (Exception e) {
					
				} 
				currentCon = null; 
				} 
			} 
		return bean; 
			
	} 
}	
